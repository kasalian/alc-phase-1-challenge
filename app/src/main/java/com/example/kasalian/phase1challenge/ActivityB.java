package com.example.kasalian.phase1challenge;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ActivityB extends AppCompatActivity {
    String URL = "https://andela.com/alc/";
//        String URL = "https://google.com";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);

        ActionBar aB = getSupportActionBar();
//        aB.setDisplayHomeAsUpEnabled(true);

        WebView wb = (WebView) findViewById(R.id.web_view);
        wb.getSettings().setJavaScriptEnabled(true);
        wb.setWebViewClient(new WebViewClient());
        wb.loadUrl(URL);
    }
}
